use log::{info, warn};
use serde::Deserialize;
use std::{
    fs::File,
    io::BufReader,
    {thread, time::Duration},
};
use ureq::Error;

static BASE_URL: &str = "domains.google.com/nic/update";
static CONFIG_FILE: &str = "config";
static HOUR_MS: u64 = 3600000;

#[derive(Deserialize)]
struct DomainData {
    username: String,
    password: String,
    domain: String,
}

fn main() {
    env_logger::init();
    loop {
        update_ip_and_domains();
        thread::sleep(Duration::from_millis(HOUR_MS));
    }
}

fn update_ip_and_domains() {
    let domain_data = get_domain_data();
    let new_ip = get_current_ip().unwrap_or_else(|| "ifconfig.me down, cannot show updated IP".to_owned());
    for domain in domain_data {
        let url = create_url(&domain.username, &domain.password, &domain.domain);
        update_dns(&url, &new_ip);
    }
}

fn get_domain_data() -> Vec<DomainData> {
    match File::open(CONFIG_FILE).and_then(|file| {
        let reader = BufReader::new(file);
        serde_json::from_reader(reader).map_err(|e| {
            warn!("Unable to read config from file: {}", e);
            std::io::Error::last_os_error()
        })
    }) {
        Ok(v) => v,
        Err(e) => {
            warn!("Unable to read in config file {}", e);
            vec![]
        }
    }
}

fn get_current_ip() -> Option<String> {
    match ureq::get("https://ifconfig.me/ip").call() {
        Ok(response) => Some(response.into_string().unwrap()),
        Err(e) => {
            warn!("Failed to get latest IP address: {}", e);
            None
        }
    }
}

fn update_dns(url: &str, new_ip: &str) {
    match ureq::get(url).call() {
        Ok(_) => info!("Successfully updated DNS for: {} to: {}", url, new_ip,),
        Err(Error::Status(code, response)) => {
            println!(
                "Failed to update DNS for: {}, code:response {}:{}",
                url,
                code,
                response.into_string().unwrap()
            )
        }
        Err(Error::Transport(e)) => println!("Failed to contact Google domains: {}", e),
    }
}

fn create_url(username: &str, password: &str, domain: &str) -> String {
    format!(
        "https://{}:{}@{}?hostname={}",
        username, password, BASE_URL, domain
    )
}
