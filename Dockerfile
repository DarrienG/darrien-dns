FROM rust:1.55-buster AS build

RUN mkdir /project
WORKDIR /project

COPY Cargo.toml ./Cargo.toml
COPY Cargo.lock ./Cargo.lock
COPY src ./src

RUN cargo build --release && strip target/release/darrien-dns

FROM debian:buster

RUN apt update -y && apt install -y dumb-init

COPY --from=build /project/target/release/darrien-dns /darrien-dns

ENTRYPOINT ["dumb-init", "--", "/darrien-dns"]