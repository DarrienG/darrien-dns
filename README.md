# Darrien DNS

Dynamic DNS wrappers just don't have great docs or are way more complicated
than they need to be. Let's do it here and be done with it.

Get binary (in releases or with `cargo build --release`) or
[docker](https://hub.docker.com/r/darrieng/darrien-dns) container.

Place a file in the same directory as the binary called `config`. It's a JSON
file.

This is what it looks like:

```json
[
    {
        "username": "myusername",
        "password": "password",
        "domain": "example.com",
    },
    {
        "username": "anotherusername",
        "password": "anotherpassword",
        "domain": "anotherdomain.com",
    },
]
```

If you're using the [docker](https://hub.docker.com/r/darrieng/darrien-dns)
build, mount the file in `/` as `config`

All done. Now every hour your domains will have their DNS updated to be your
internet accessible IP address.

# Q/A

Q. This only works with Google domains?

![yes](assets/chad.png)
